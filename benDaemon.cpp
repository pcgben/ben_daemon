#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <map>
#include <regex>
// daemon functions
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
// logs
#include <fstream>
#include <chrono>
#include <ctime>
#include <iomanip>

void logOutput(std::string);

static void daemon();

std::string exec(const char* cmd);

enum EventAction {      noEventInQueue,
                        processEvent };

static std::map<std::string, EventAction> mapValueToEvent;

void initialise();

int main() {
    syslog(LOG_ERR, "bendaemon has started");
    daemon();
    initialise();

    for(int i = 0; i < 20; ++i) {
        std::string result = exec("php -f /Users/easysigns/PlayGround/bash/readPhpOutput/testOutput.php");
        switch(mapValueToEvent[result]) {
            case processEvent:
                // process event one
                logOutput("one");
                break;
            case noEventInQueue:
                // write some event complete log
                logOutput("zero");
                sleep(20);
                break;
            default:
                // event not defined.  write some error log
                break;
        }
    }

    syslog(LOG_ERR, "Terminating daemon");
    closelog();
    return EXIT_SUCCESS;
}

std::string exec(const char* cmd) {
    std::array<char, 4096> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
        while (!feof(pipe.get())) {
            if (fgets(buffer.data(), 4096, pipe.get()) != nullptr) {
                result += buffer.data();
            }
        }
        std::regex expression("EXITCODE-0");
        if(std::regex_search(result, expression) == 0) {
            result = "0";
        }
    else {
        result = "1";
    }
    return result;
}

void initialise() {
    mapValueToEvent["0"] = noEventInQueue;
    mapValueToEvent["1"] = processEvent;
}

void logOutput(std::string value) {
    std::ofstream logFile;
    logFile.open("/Users/easysigns/PlayGround/bash/readPhpOutput/logFile.txt", std::ios_base::app);
    std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    logFile << "Time: " << std::put_time(std::localtime(&now), "%F %T") <<  " Event success: " << value << std::endl;
    return;
}

static void daemon() {
    pid_t pid; 
    // Fork off the parent process
    pid = fork();
    // An error occurred
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    // Success: Let the parent terminate
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }   
    // On success: The child process becomes session leader
    if (setsid() < 0) {
        exit(EXIT_FAILURE);
    }
    //TODO: Implement a working signal handler
    signal(SIGCHLD, SIG_IGN);
    signal(SIGHUP, SIG_IGN);
    // Fork off for the second time (detatch from terminal window)
    pid = fork();
    // An error occurred
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    // Success: Let the parent terminate
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    umask(0);
    // This may need to point to a different directory in the future
    chdir("/");
    // Close all open file descriptors
    for (int x = (int)sysconf(_SC_OPEN_MAX); x >= 0; x--) {
        close (x);
    }
    // Open the log file
    openlog("bendaemon", LOG_PID, LOG_DAEMON);
}
